# -*- coding: utf-8 -*-
"""
Created on Thu May 14 21:57:36 2020

@author: MManiraj
"""

import dash
import dash_html_components as html
import dash_core_components as dcc
import pandas as pd  
import plotly.express as px
import plotly.graph_objs as go
import psycopg2
import dash_table
import safe_password as sp
from dash.dependencies import Input, Output


input_data = 319146855,319146935,319146875,434091087,434091271,434091304,434094187
connection = psycopg2.connect(user='mmaniraj', password=sp.readpw("py1usta1"), host='orlpybvip01.catmktg.com', port='5432', database='py1usta1') 
cursor = connection.cursor()

query1='''  select pv.promo_src_id as mclu_nbr,
sum(f.tot_selected_qty) as dist_qty,
tp.cntry_subdiv_cd as state_cd, tp.lgl_entity_nm as parent_corp_nm
from ord_promo_varnt_fact_v f
join date_v d on d.date_key = f.ord_date_key and
f.event_typ_cd in( 'IS-PRINT-T','IS-SPRINT-T')
join promotion_variant_v pv on 
pv.promo_varnt_key = f.promo_varnt_key  and
pv.promo_src_id in ({BL})
join touchpoint_v tp on tp.touchpoint_key = f.ord_touchpoint_key
group by 1,3,4 order by 1'''.format(BL=str(input_data).strip("()"))
df1 = pd.read_sql(query1, connection)   #execute query
#df1
mclist=df1['mclu_nbr'].unique().tolist()
global geoflg
geoflg = all

external_stylesheets = ['/assets/bWLwgP.css']
colors = {
    'background': '#111111',
    'text': '#7FDBFF',
     'text1': '#7777FF',
     'white_t': '#FFFFFF'
}
import json
with open('geojson-counties-fips.json') as f:
    counties = json.load(f)
df3_geo = pd.read_csv("fips-unemp.csv",
                   dtype={"fips": str})
fig = px.choropleth(df3_geo, geojson=counties, locations='fips', color='unemp',
                           color_continuous_scale="Viridis",
                           range_color=(0, 12),
                           scope="usa",
                           labels={'unemp':'unemployment rate'},animation_frame="year", animation_group="fips")
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})


app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.layout = html.Div([
        html.H2(
        children='Demo application for Analytics',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),     html.Div([
        html.H5(children='MCLU NBR'),
        dcc.Checklist(
            id='mclu',
            options=[{'label':i , 'value':i} for i in mclist],  
            value = [''],
            labelStyle={'display': 'inline-block'}),
           html.Div([
               dcc.Graph(id='geo-chart') ]
            , style={'height': '50%', 'width': '45%', 'float': 'left', 'display': 'inline-block'}),
            html.Div([
        dcc.Graph(id='pie-chart')]
             , style={'height': '40%', 'width': '35%', 'float': 'left', 'display': 'inline-block'}),
            html.Div([
        dcc.Graph(id='pie-chart2')]
             , style={'height': '40%', 'width': '20%', 'float': 'right', 'display': 'inline-block'})

        ]), html.Div([dash_table.DataTable(
                   id='table1',
                   columns=[{"name": i, "id": i} for i in df1.groupby(['mclu_nbr'], as_index=False)['dist_qty'].sum().columns],
                   data=df1.groupby(['mclu_nbr'], as_index=False)['dist_qty'].sum().to_dict('records')
                               )]
    , style={'width': '50%', 'display': 'inline-block'}),
html.Div([dcc.Graph(id='graph-unemp', figure = fig ) ], 
    style={'height': '50%', 'width': '48%', 'float': 'right', 'display': 'inline-block'})
])


@app.callback(
    [Output('pie-chart','figure'),Output('pie-chart2','figure')],
    [Input('mclu','value'),Input('geo-chart', 'clickData')] )
def update_graph(mclus,clk_dt):
    global geoflg
    if geoflg == clk_dt:
        clk_dt = None
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if changed_id!='geo-chart.clickData':
        clk_dt = None
    geoflg = clk_dt
    if mclus == [''] and clk_dt is None:
        dfg= df1.groupby(['parent_corp_nm'], as_index=False)['dist_qty'].sum()
        df3 = df1.groupby(['state_cd'], as_index=False)['dist_qty'].sum()
    elif clk_dt is None:
        dfg=df1[df1['mclu_nbr'].isin(mclus)].groupby(['parent_corp_nm'], as_index=False)['dist_qty'].sum()
        df3=df1[df1['mclu_nbr'].isin(mclus)].groupby(['state_cd'], as_index=False)['dist_qty'].sum()
    elif mclus == ['']:
        dfg=df1[df1['state_cd'] == clk_dt['points'][0]['location']].groupby(['parent_corp_nm'], as_index=False)['dist_qty'].sum()
        df3=df1[df1['state_cd'] == clk_dt['points'][0]['location']].groupby(['state_cd'], as_index=False)['dist_qty'].sum()
    else:
        dfg=df1[(df1['mclu_nbr'].isin(mclus))&(df1['state_cd'] == clk_dt['points'][0]['location'])].groupby(['parent_corp_nm'], as_index=False)['dist_qty'].sum()
        df3=df1[(df1['mclu_nbr'].isin(mclus))&(df1['state_cd'] == clk_dt['points'][0]['location'])].groupby(['state_cd'], as_index=False)['dist_qty'].sum()
    fig = px.pie(df3, values='dist_qty', names='state_cd',title=f"Distribution Report by State: "+ "Total USA" if clk_dt is None else 
                            f"Distribution Report by State: "+clk_dt['points'][0]['location'])
    fig.update_traces(textposition='inside', textinfo='percent+label')
    fig.update_layout(uniformtext_minsize=12, uniformtext_mode='hide')
    return {
        "data": [go.Pie(labels=dfg['parent_corp_nm'].unique().tolist(), 
                       values=dfg['dist_qty'])],
        "layout": go.Layout(title=f"Retailer Distribution Report for "+ "Total USA" if clk_dt is None else f"Retailer Distribution Report for "+clk_dt['points'][0]['location'] )
}, fig 
#{
#         "data": [go.Pie(labels=df3['state_cd'].unique().tolist(), values=df3['dist_qty'])],
#        "layout": go.Layout(title=f"Distribution Report by State: "+ "Total USA" if clk_dt is None else 
#                            f"Distribution Report by State: "+clk_dt['points'][0]['location'])
#}

@app.callback(
    Output('geo-chart','figure'),
    [Input('mclu','value')] )
def update_graph1(mclus):
    if mclus == ['']:
        df3 = df1.groupby(['state_cd'], as_index=False)['dist_qty'].sum()
    else:
        df3=df1[df1['mclu_nbr'].isin(mclus)].groupby(['state_cd'], as_index=False)['dist_qty'].sum()
    return {
        "data": [go.Choropleth(
    locations=df3['state_cd'], # Spatial coordinates
    z = df3['dist_qty'].astype(float), # Data to be color-coded
    locationmode = 'USA-states', # set of locations match entries in `locations`
    colorscale = 'Blues',
    colorbar_title = "Coupon counts",
)],
        "layout": go.Layout(title=f"Coupon Distribution by State", geo_scope='usa')
}


if __name__ == '__main__':
    app.run_server(port = '8060', debug=True)

