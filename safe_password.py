# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 06:39:49 2019
Version 1.10 Release
@author: Mannar Maniraj

Modified on Fri Feb 28 10:30:00 2020
Fixed the Linux path -- to /home/<username>/
Python Module to safely handle passwords. 

Assumptions and requirement: "X:\" drive (path: \\stphome\home\<UserID>)  is mapped for Windows operating system 
and "/home" for Linux or unix.
"x:\"  is a default drive mapped by for all users. By default only the user has access to this drive.
#each time you have to retrive your password for use in your code.
import safe_password as sp
pwd = sp.readpw("py1usta1")

"""
#Parameters
dbname = "pn1uspa1"
pwd = "password"


#main program
import sys
from os import path
import getpass

def createpwdf(dbname="d", pwd="p"):
    if sys.platform.startswith('linux') and path.isdir('/home/'+getpass.getuser()+'/'):
        pwd_path = '/home/'+getpass.getuser()+'/'
    elif sys.platform in ['win32','cygwin'] and path.isdir('x:/'):
        pwd_path = 'x:/'
    else:   
        print("Could not find default location on the system: x:/ or /home/"+getpass.getuser()+"/")
        return ""
    f = open( pwd_path + dbname + ".pwd", "w")
    f.write(pwd)
    f.close()
    return print("Password file for database "+ dbname + " created.")

if __name__ != "__main__": 
#    createpwdf(dbname, pwd)
#else:
    def readpw(dbname="database name"):
        if sys.platform.startswith('linux') and path.isdir('/home/'+getpass.getuser()+'/'):
            pwd_path = '/home/'+getpass.getuser()+'/'
        elif sys.platform in ['win32','cygwin'] and path.isdir('x:/'):
            pwd_path = 'x:/'
        else:   
            print("Could not find default location on the system: x:/ or /home/"+getpass.getuser()+"/")
            return ""
        try:
            f = open( pwd_path + dbname + ".pwd", "r")
            pwd = f.read()
            f.close()
            return pwd
        except FileNotFoundError:
            print("Password for the database name: " + dbname + " not avaliable. " + pwd_path +" \nUse createpwdf(dbname) function to create the password file. If already created spellcheck dbname.")
            print("Do you want to create the password file: Y/N:")
            if input(prompt='Enter "Y" or "N" :').lower() == 'y':
                pwd = getpass.getpass(prompt='Enter the database password :')
                f = open( pwd_path + dbname + ".pwd", "w")
                f.write(pwd)
                f.close()
                return pwd
            else: 
                print ("Password for the database name: " + dbname + " could not be retrieved")
                return ""
        except:
            print("Password could not be retrieved, Error:" , sys.exc_info()[0])
            return ""