# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 12:36:25 2019

@author: mmaniraj
"""
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output

import pandas as pd
import plotly.graph_objs as go

df = pd.read_excel('c:/Users/mmaniraj/Documents/Webpage/IRI_Category_Brand_Sales_Q1_2019.xlsx', index_col=None)
dff3 = df[df['Category'] == 'AIR FRESHENERS'].groupby(['Product'], as_index=False)['Dollar Sales','Dollar Share of Category'].sum()
available_Categories = df['Category'].unique()
available_subcat = df['Product'].unique()

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
colors = {
    'background': '#111111',
    'text': '#7FDBFF',
     'text1': '#7777FF'
}

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.layout = html.Div([
            html.H1(
        children='2019 Q1 IRI Category Brand Sales Dashboard',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),
           html.Div([
            html.H4(children='Category:'),
            dcc.Dropdown(
                id='category-column',
                options=[{'label': i, 'value': i} for i in available_Categories],
                value='Category'
            )]
        ,style={'width': '48%', 'display': 'inline-block'}),

    html.Div([
            html.H5(children='Dollar Share of Category' , style={
            'textAlign': 'center'
        }),
            dcc.Graph(id='graph-pie'), 
            ] 
            , style={'height': '55%', 'width': '48%', 'float': 'right', 'display': 'inline-block'})
,  html.Div([dash_table.DataTable(
                   id='table',
                   columns=[{"name": i, "id": i} for i in dff3.columns],
                   data=[{}]
                               )]
    , style={'width': '48%', 'display': 'inline-block'}),
   html.H4(children='Sub Category:'),
   html.Div([
            dcc.Dropdown(
                id='subcategory-column',
                options=[{'label': i, 'value': i} for i in available_subcat],
                value='Sub Category'
            )]
            ,style={'width': '48%', 'display': 'inline-block'})
    ,
     html.Div([
        dcc.Graph(id='treemap', 
        config=dict(displayModeBar=False)),            
        ] 
      , style={'width': '95%', 'display': 'inline-block','float': 'right' })
])

@app.callback(
    Output('subcategory-column', 'options'),
    [Input('category-column', 'value')])
def update_dropd(cat_value):
    dff = df[df['Category'] == cat_value]
    sbcat_opt = dff['Product'].unique()
    return [{'label': i, 'value': i} for i in sbcat_opt]

@app.callback(
        Output('graph-pie', 'figure'),
        [Input('category-column', 'value')])
def update_graphp(cat_value):
    dff3 = df[df['Category'] == cat_value].groupby(['Product'], as_index=False)['Dollar Sales'].sum()
    return {
         'data': [
                {'labels':dff3['Product'],'values':dff3['Dollar Sales'], 'type': 'pie'}
            ],
            'layout': go.Layout(showlegend=True)
            }
@app.callback(
        Output('table', 'data'),
        [Input('category-column', 'value')])
def update_table(cat_value):
    dff3 = df[df['Category'] == cat_value].groupby(['Product'], as_index=False)['Dollar Sales','Dollar Share of Category'].sum().round({'Dollar Share of Category' : 2, 'Dollar Share of SubCategory':2})
    dff3['Dollar Sales']=dff3['Dollar Sales'].map('${:,}'.format)
    dff3['Dollar Share of Category']=dff3['Dollar Share of Category'].map('{:.2f}%'.format)
    return dff3.to_dict('records')

@app.callback(
        Output('treemap', 'figure'),
        [Input('category-column', 'value'),
         Input('subcategory-column','value')])
def update_treemap(cat_value,subcat_value):
    dff3 = df[(df['Category'] == cat_value) & (df['Product'] == subcat_value)].groupby(['Major Brand'], as_index=False)['Dollar Sales'].sum().sort_values(by='Dollar Sales', ascending =True)
    return {
         'data': [
                {'x':dff3['Dollar Sales'], 'y':dff3['Major Brand'] , 'type': 'bar', 'name': 'Dollar Sales', 'orientation': 'h'}
            ],
            'layout': {
#                'plot_bgcolor': colors['background'],
#                'paper_bgcolor': colors['background'],
                'font': {
                    'color': colors['text1']
                }
            }
            }     

# change these values
PORT = '8000'
ADDRESS = '127.0.0.1'
if __name__ == '__main__':
    app.run_server(port=PORT,
        host=ADDRESS,debug=True)