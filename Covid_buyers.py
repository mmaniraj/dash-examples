import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output

import pandas as pd
import plotly.graph_objs as go
df = pd.read_excel('/sasuser/network_shared/Symphony/vsriniva/Python Project - Dash/Cat_data.xlsx', index_col=None)
#df.head()
available_Categories = df['Mjr_Cat_desc'].unique()
table = pd.DataFrame(columns=['Description', '26wk_new_ids', '52wk_new_ids','covidprd_ids_total','yago_covidprd_ids_total'])
table2 = pd.DataFrame(columns=['Category - Pct_Change', 'Own Brand - Pct_Change'])
external_stylesheets = ['/assets/bWLwgP.css']
colors = {
    'background': '#111111',
    'text': '#7FDBFF',
     'text1': '#7777FF'
}

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
#header
app.layout = html.Div([
    html.Div([html.H1("Meijer Brand / Categories New Shoppers - COVID Period")]
             , style={"textAlign": "center",'color': colors['text']}),
 
#category list   
     html.Div([
        html.H2(children='Meijer Category:'),
        
         dcc.Dropdown(
            id='category-column',
            options=[{'label':i , 'value':i} for i in available_Categories],  
            value =df['Mjr_Cat_desc'].iloc[0],
            style={'width': '45%','display': 'inline-block'}
        )

 #data table Pct Change       
           ,html.Div([dash_table.DataTable(
               id='table1',
               columns=[{'name': i, 'id': i} for i in table2.columns],
               data=[{}]
              )]
         ,style={'width': '20%', 'float': 'right', 'fontSize':16, 'display': 'inline-block'
                ,'marginLeft':50,'marginRight':20,'marginTop':10,'marginBottom':0}) #'font-family':'sans-serif',

#data table Category & Own Brand      
           ,html.Div([dash_table.DataTable(
               id='table',
               columns=[{'name': i, 'id': i} for i in table.columns],
               data=[{}]
              )]
         ,style={'width': '48%', 'float': 'left', 'fontSize':16, 'display': 'inline-block'}) #'font-family':'sans-serif',
         
#Bar graph 1       
             ,html.Div([
        dcc.Graph(id='graph-bar1', 
        config=dict(displayModeBar=False)),            
        ] 
            , style={'width': '48%','height':'48%','float': 'right', 'display': 'inline-block'
                         ,'marginLeft':20,'marginRight':10,'marginTop':50,'marginBottom':50 ,
                        'border':'thin black dashed'})
         
#pie graph 2       
         , html.Div([
            html.H6(children='New Households % - Category' , style={'textAlign': 'center','fontSize':20}),
            dcc.Graph(id='graph-pie2'),] 
            , style={'width': '23%','height':'23%','float': 'left', 'display': 'inline-block'
                         ,'marginLeft':10,'marginRight':10,'marginTop':20,'marginBottom':10 
                        ,'border':'thin black dashed'})
         
 #pie graph 1        
               , html.Div([
            html.H5(children='New Households % - Own Brand' , style={'textAlign': 'center','fontSize':20}),
            dcc.Graph(id='graph-pie1'),] 
            , style={'width': '23%','height':'23%','float': 'left', 'display': 'inline-block'
                         ,'marginLeft':10,'marginRight':10,
                        'marginTop':20,'marginBottom':10,
                        'border':'thin black dashed'})
 
     
     ])
])

@app.callback(
        Output('table', 'data'),
        [Input('category-column', 'value')])
    
def update_table(value):
    table1 = pd.DataFrame(columns=['Description', '26wk_new_ids', '52wk_new_ids','covidprd_ids_total','yago_covidprd_ids_total'])
    table1 = table1.append({'Description': 'Category', '26wk_new_ids':f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 5]:,d}",
                      '52wk_new_ids':f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 6]:,d}",
                      'covidprd_ids_total': f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 7]:,d}", 
                      'yago_covidprd_ids_total' : f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 8]:,d}"
                     },ignore_index=True)
    table1 = table1.append({'Description': 'Own Brand', '26wk_new_ids':f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 1]:,d}",
                      '52wk_new_ids':f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 2]:,d}",
                      'covidprd_ids_total': f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 3]:,d}", 
                      'yago_covidprd_ids_total' : f"{df[df['Mjr_Cat_desc'] == value].iloc[0, 4]:,d}"
                     },ignore_index=True)
    return table1.to_dict('records')

@app.callback(
        Output('table1', 'data'),
        [Input('category-column', 'value')])
    
def update_table(value):
    table2 = pd.DataFrame(columns=['Category - Pct_Change', 'Own Brand - Pct_Change'])
    dff5 = df[df['Mjr_Cat_desc'] == value].groupby(['Mjr_Cat_desc'], 
        as_index=False)['Total_COVID_IDS_Cat','TOTAL_YAGO_COVID_IDS_Cat','Total_COVID_IDS_OB','TOTAL_YAGO_COVID_IDS_OB'].sum()
    dff5['pct_change_Cat'] = (dff5['Total_COVID_IDS_Cat']-dff5['TOTAL_YAGO_COVID_IDS_Cat'])/dff5['TOTAL_YAGO_COVID_IDS_Cat']*100
    dff5['pct_change_Cat'] = dff5['pct_change_Cat'].map('{:.2f}%'.format)
    dff5['pct_change_OB'] = (dff5['Total_COVID_IDS_OB']-dff5['TOTAL_YAGO_COVID_IDS_OB'])/dff5['TOTAL_YAGO_COVID_IDS_OB']*100
    dff5['pct_change_OB'] = dff5['pct_change_OB'].map('{:.2f}%'.format)
    index_ = ['Mjr_Cat_desc']
    dff5.index = index_
    df4=dff5.T
    header=df4.iloc[0]
    df4=df4[1:]
    df4.columns=header
    table2 = table2.append({'Category - Pct_Change': df4[value][4], 'Own Brand - Pct_Change': df4[value][5]
                     },ignore_index=True)
    return table2.to_dict('records')

@app.callback(
        Output('graph-pie1', 'figure'),
        [Input('category-column', 'value')])
def update_graphp(value):
    dff3 = df[df['Mjr_Cat_desc'] == value].groupby(['Mjr_Cat_desc'], as_index=False)['52wk_new_ids_OB','Total_COVID_IDS_OB'].sum()
    index_ = ['Mjr_Cat_desc']
    dff3.index = index_
    df2=dff3.T
    header=df2.iloc[0]
    df2=df2[1:]
    df2.columns=header
    return {
        "data": [go.Pie(labels=['52wk_new_ids_OB','Existing_IDS_OB'], values=[df2[value][0] , df2[value][1]])]
            ,'layout': go.Layout(showlegend=True,legend_orientation="h")
             }   

@app.callback(
        Output('graph-pie2', 'figure'),
        [Input('category-column', 'value')])
def update_graphp(value):
    dff4 = df[df['Mjr_Cat_desc'] == value].groupby(['Mjr_Cat_desc'], as_index=False)['52wk_new_ids_Cat','Total_COVID_IDS_Cat'].sum()
    index_ = ['Mjr_Cat_desc']
    dff4.index = index_
    df3=dff4.T
    header=df3.iloc[0]
    df3=df3[1:]
    df3.columns=header
    return {
       "data": [go.Pie(labels=['52wk_new_ids_Cat','Existing_IDS_Cat'], values=[df3[value][0] , df3[value][1]])]              
            ,'layout': go.Layout(showlegend=True,legend_orientation="h")
             }

@app.callback(
        Output('graph-bar1', 'figure'),
        [Input('category-column', 'value')])
def update_graphbar1(value):
    dff5 = df[df['Mjr_Cat_desc'] == value].groupby(['Mjr_Cat_desc'], 
        as_index=False)['Total_COVID_IDS_Cat','TOTAL_YAGO_COVID_IDS_Cat','Total_COVID_IDS_OB','TOTAL_YAGO_COVID_IDS_OB'].sum()
    index_ = ['Mjr_Cat_desc']
    dff5.index = index_
    df4=dff5.T
    header=df4.iloc[0]
    df4=df4[1:]
    df4.columns=header
        
    fig_t = go.Figure(go.Bar(x=['TY_HH_IDS','YAGO_HH_IDS'], y=[df4[value][0],df4[value][1]], name = 'Category Share', marker_color='mediumblue', opacity=0.5,text=[df4[value][0],df4[value][1]],textposition='auto'))
    fig_t.add_trace(go.Bar(x=['TY_HH_IDS','YAGO_HH_IDS'], y=[df4[value][2],df4[value][3]],  name = 'OB Share', marker_color='gray', opacity=0.8,text=[df4[value][2],df4[value][3]],textposition='auto'))
    fig_t.update_layout(barmode='overlay', 
                    xaxis={'categoryorder':'total ascending'},xaxis_type='category',showlegend=True,legend_orientation="h"
                  ,title={'text': 'Household Shopping Trip - Covid Period','x':0.45,'xanchor': 'center','yanchor': 'top'}
       ,)
    
    return fig_t


if __name__ == '__main__':
    app.run_server(host='10.21.127.18',port='8055',debug=True, use_reloader=False)