# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 11:04:16 2020

@author: MManiraj
"""

import dash
from dash.dependencies import Input, Output, State
#from dash.exceptions import PreventUpdate
import dash_html_components as html
import dash_core_components as dcc
import dash_table
import psycopg2 as ybconn
import pandas as pd  
import safe_password as sp
import datetime
import lmc_list_upc as lmc

import base64
import io
import os

UPLOAD_DIRECTORY = "/sasuser/network_shared/Symphony/mmaniraj/LMC/"

if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)

val =""

external_stylesheets = ['/assets/bWLwgP.css']
colors = {
    'background': '#111111',
    'text': '#7FDBFF',
     'text1': '#7777FF',
     'white_t': '#FFFFFF'
}

#Function to send the job completion mail
def email_macro(msg_txt,subj="This is TEST MAIL from Python",To_list="mannar.maniraj@catalina.com"):
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    smtpObj = smtplib.SMTP(host='outlookorl.catalinamarketing.com', port=25)
    MY_ADDRESS = 'mannar.maniraj@catalinamarketing.com'
    smtpObj.starttls()
    msg = MIMEMultipart()       # create a message
    msg['From']=MY_ADDRESS
    msg['To']=To_list
    msg['Subject']=subj
    msg.attach(MIMEText(msg_txt, 'plain'))
    smtpObj.send_message(msg)
    del msg
    smtpObj.quit()
#Function to prepare a mapping file for joining the list and select brand and category UPC group
def prep_map_fl(input_df):
    out_df = pd.DataFrame({'autonbr':[],'UPC_group': [], 'LMC_UPC_List': [],
                       'lmc_groups': [], 'brand_nbr': [] })
    for rw in input_df.index:
        if type(input_df['lmc_category_groups'][rw]) == str:
            for n in input_df['lmc_category_groups'][rw].split(","):
                out_df =  out_df.append({'autonbr':input_df['autonbr'][rw],'UPC_group':'CATEGORY','LMC_UPC_List':input_df['LMC_UPC_List'][rw],'lmc_groups':input_df['lmc_category_groups'][rw],'brand_nbr':n},ignore_index=True)
        else:
            out_df =  out_df.append({'autonbr':input_df['autonbr'][rw],'UPC_group':'CATEGORY','LMC_UPC_List':input_df['LMC_UPC_List'][rw],'lmc_groups':input_df['lmc_category_groups'][rw],'brand_nbr':input_df['lmc_category_groups'][rw]},ignore_index=True)
        if type(input_df['lmc_brand_groups'][rw]) == str:
            for m in input_df['lmc_brand_groups'][rw].split(","):
                out_df =  out_df.append({'autonbr':input_df['autonbr'][rw],'UPC_group':'BRAND','LMC_UPC_List':input_df['LMC_UPC_List'][rw],'lmc_groups':input_df['lmc_brand_groups'][rw],'brand_nbr':m},ignore_index=True)
        else:
            out_df =  out_df.append({'autonbr':input_df['autonbr'][rw],'UPC_group':'BRAND','LMC_UPC_List':input_df['LMC_UPC_List'][rw],'lmc_groups':input_df['lmc_brand_groups'][rw],'brand_nbr':input_df['lmc_brand_groups'][rw]},ignore_index=True)
    return out_df        
#Function to create error message
def err_txt(err_txt,msg):
    if err_txt =="":
        err_txt = "\nFollowing lists couldnot be processed\n\t" + msg
    else:
        err_txt = err_txt + "\n\t"+ msg
    return err_txt

#Function to process the LMC UPC using the uploaded files
def lmc_process(inpt_fl, t):
    from datetime import datetime
    try:
        dobj = datetime.now()
        err_note=""
        out_pth =r'/sasuser/network_shared/Symphony/mmaniraj/LMC' 
        filename = out_pth +'/LMC_UPC_List_'+str(dobj.date())+t+'.csv'
        mapfilename = out_pth+'/LMC_MAPPING_File_'+str(dobj.date())+t+'.csv'
        err_note=""
        message = " *** This is an automated message ***\n\n Hello, \n\nSuccessfully downloaded all LMC UPC list, refer details below;\n\n\tInput file: "+inpt_fl
        message=message+ "\n\tOutput file:"+filename+"\n\tMapping file:"+mapfilename
        df = pd.read_excel(inpt_fl, index_col=None)
        map_df = prep_map_fl(df)
        map_df.to_csv(mapfilename, index=False)
        message=message+"\n\tImported LMC list id count:"+ str(len(df['LMC_UPC_List'])) + " records. \n\tUnique Lists count :" + str(len(df['LMC_UPC_List'].unique()))+ " records."
        mylist = pd.DataFrame({'UPC_CD': [], 'LMC_UPC_List': [],
                           'brand_name': [], 'brand_nbr': [],  'LMC_UPC_LIST_NAME': [] })
        mylist.to_csv(filename, index=False)
        LMC_BaseURL="https://listmanager.catalinamarketing.com"
        for lst in df['LMC_UPC_List'].unique():
            if type(lst) == str:
                if len(lst) == 36:
                    try:
                        mylist = lmc.LMC_PandasGetAllUPCs(LMC_BaseURL, lst)
                        mylist = mylist[['upc_cd','LMC_UPC_List','group_name', 'group_number','LMC_LIST_NAME']]
                        mylist.to_csv(filename, index=False, mode = 'a', header=False)
                    except:
                        err_note = err_txt(err_note,str(lst))
                else:
                    err_note = err_txt(err_note,str(lst)+ " :invalid list number, Lenth is less than 36 characters.")
            else:
                err_note = err_txt(err_note,str(lst)+ " :invalid list number, appears to be blank")        
        subject="Completed: LMC UPC list download " +str(dobj.date())+t
        to = "mannar.maniraj@catalina.com; Russell.Brown@catalina.com"
        message = message + "\n\tJob started at "+str(dobj)
        message=message+"\n\tJob ended at " + str(datetime.now()) +"\n"+err_note+"\n\nThank you!!\n\n ****** End of the Message ******* "
        email_macro(message,subject,to)
        return subject+"\n"+message
    except Exception as e:
        print(e)
        return "Something is wrong!!. Please upload valid import file."

#Function to pull YB monitor code
def pull_usage():
    connection = ybconn.connect(user='mmaniraj', password=sp.readpw("py1uspa1"), host='orlpybvip01.catmktg.com', port='5432', database='py1uspa1')
    query1='''select User_name, query_id, submit_time, type, round(tot_sec/60,1)::int as tot_mins, round(que_sec/60,1)::int as que_min, 
    plan_sec, exec_sec, lock_sec, spill_mb, max_mb, query_text, transaction_id, session_id, 
    pool_id, status  from query_history_p() where query_id > 0 order by  tot_mins desc'''
    global df
    df = pd.read_sql(query1, connection)   #execute query
    connection.close()

def save_file(name, content):
    global val
    """Decode and store a file uploaded with Plotly Dash."""
    if 'xls' in name or 'csv' in name:
        data = content.encode("utf8").split(b";base64,")[1]
        with open(os.path.join(UPLOAD_DIRECTORY, name), "wb") as fp:
            fp.write(base64.decodebytes(data))
            val = os.path.join(UPLOAD_DIRECTORY, name)
    return val
def parse_contents(contents, filename, date):
    #df45 = pd.DataFrame({'user_name': [], 'submit_time': [], 'type': [], 'tot_sec': [], 'tot_mins': [], 'que_sec': [] })
    try:
        content_type, content_string = contents.split(',')
        decoded = base64.b64decode(content_string)
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df45 = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')))
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df45 = pd.read_excel(io.BytesIO(decoded))
        else:
            return html.Div([html.P(
            'Invalid file format, please upload only .xls file.')
            ])
    except Exception as e:
        print(e)
        return html.Div([html.P(
            'There was an error processing this file.')
        ])

    return html.Div([
        html.H6(filename),
        dash_table.DataTable(
            data=df45.to_dict('records'),
            columns=[{'name': i, 'id': i} for i in df45.columns]
        ),

        html.Hr(),  # horizontal line

        # For debugging, display the raw contents provided by the web browser
#        html.Div('Raw Content'),
#        html.Pre(contents[0:200] + '...', style={
#            'whiteSpace': 'pre-wrap',
#            'wordBreak': 'break-all'
#        })
    ])

pull_usage()

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.layout = html.Div([
        html.H2(
        children='YellowBrick usage Monitor tool',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),
        dcc.Tabs(id="tabs", value='tab-1', children=[
        dcc.Tab(label='Monitor Tab', value='tab-1', children = [
html.Button(id='refresh-button', n_clicks=0, children='Refresh'),
      html.Hr(),       
  html.Div([dash_table.DataTable(  style_data={
         'whiteSpace': 'normal',
        'height': 'auto'
    },     style_cell_conditional=[
        {
            'if': {'column_id': 'query_text'},
            'textAlign': 'left'
        }
    ],style_cell={
        'minWidth': '0px', 'maxWidth': '950px',

    },
                   id='table1',
                   columns=[{"name": i, "id": i} for i in df.columns],
                   data=df.to_dict('records')
                               )]
    , style={'width': '95%', 'display': 'inline-block'}),
    html.Div(id='time-section',
        style={
            'textAlign': 'Right',
            'color': colors['text1']
        }),
 html.Label ('Time dashboard was started: ' + datetime.datetime.now().strftime("%c"),
        style={
            'textAlign': 'Right',
            'color': colors['white_t']
        }),]),
        dcc.Tab(label='LMC list for COVID-9', value='tab-2', children=[
                dcc.Input(id='input_pth', placeholder="Input file name & Path:",value='', type='text',size ='100'),
                dcc.Input(id='batch_txt', value='',placeholder="batch initial", type='text',maxLength=2),
                dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True
    ), html.Button(id='pull-button', n_clicks=0, children='Pull LMC UPCs'),
    html.Div(id='output-data-upload')]),
    ]),
#    html.Div(id='tabs-content'), 
])


@app.callback(
        [Output('table1', 'data'), Output('time-section','children')],
        [Input('refresh-button', 'n_clicks')])
def update_table(t):
    pull_usage()
    x = datetime.datetime.now()
    return df.to_dict('records'), 'Last Refresh Time: ' + x.strftime("%c")
#@app.callback(Output('tabs-content', 'children'),
#              [Input('tabs', 'value')])
#def render_content(tab):
#    x = datetime.datetime.now()
#    if tab == 'tab-1':
#        return html.Div([
#            html.P('Last Refresh Time: ' + x.strftime("%c"))
#        ])
#    elif tab == 'tab-2':
#        return html.Div([
#            html.P('Last Refresh Time: ' + x.strftime("%c"))
#        ])
@app.callback([Output('output-data-upload', 'children'), Output('input_pth', 'value')], 
              [Input('upload-data', 'contents'),Input('pull-button', 'n_clicks'), Input('batch_txt', 'value')],
              [State('upload-data', 'filename'),
               State('upload-data', 'last_modified')])
def update_output(list_of_contents, nclicks, batch, list_of_names, list_of_dates):
    global val
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if list_of_names is not None and list_of_contents is not None and 'pull-button' not in changed_id and 'batch_txt' not in changed_id:
        children = [
            parse_contents(c, n, d) for c, n, d in
            zip(list_of_contents, list_of_names, list_of_dates)]
        for name, data in zip(list_of_names, list_of_contents ):
            val = save_file(name, data)
        return children, val
    if 'pull-button' in changed_id and val != "":
        msg = lmc_process(val,batch)
        val = ''
        return html.Div([html.P(msg)]), val
    return html.Div([html.P('Upload files or Enter file path in the input text box')]), val

if __name__ == '__main__':
    app.run_server(host='10.21.127.18',debug=True,use_reloader=False)
